﻿using UnityEngine;
using UnityEditor;
using System.Collections;

public class spriteAnimator : EditorWindow
{

    public static Object selectedObject;
    int numAnimations;
    string controllerName;
    string[] animationNames = new string[100];
    float[] clipFrameRate = new float[100];
    float[] clipTimeBetween = new float[100];
    int[] startFrames = new int[100];
    int[] endFrames = new int[100];
    bool[] pingPong = new bool[100];
    bool[] loop = new bool[100];

    [MenuItem("Project Tools/2D Animations")]

    // Use this for initialization
    static void Init()
    {
        selectedObject = Selection.activeObject;
        if (selectedObject == null)
            return;

        spriteAnimator window = (spriteAnimator)EditorWindow.GetWindow(typeof(spriteAnimator));
        window.Show();
    }

    void OnGUI()
    {
        EditorGUILayout.TextField("Sample Text: ");
        if (selectedObject != null)
        {
            EditorGUILayout.LabelField("Animations for " + selectedObject.name);
            EditorGUILayout.Separator();

            controllerName = EditorGUILayout.TextField("Controller name", controllerName);
            numAnimations = EditorGUILayout.IntField("How many animations?", numAnimations);


            for (int i = 0; i < numAnimations; i++)
            {
                animationNames[i] = EditorGUILayout.TextField("Animation Name", animationNames[i]);
                EditorGUILayout.BeginHorizontal();
                startFrames[i] = EditorGUILayout.IntField("Start Frame", startFrames[i]);
                endFrames[i] = EditorGUILayout.IntField("End Frame", endFrames[i]);
                EditorGUILayout.EndHorizontal();
                EditorGUILayout.BeginHorizontal();
                clipFrameRate[i] = EditorGUILayout.FloatField("Frame Rate", clipFrameRate[i]);
                clipTimeBetween[i] = EditorGUILayout.FloatField("Frame Spacing", clipTimeBetween[i]);
                EditorGUILayout.EndHorizontal();
                //EditorGUILayout.BeginHorizontal();

                EditorGUILayout.BeginHorizontal();
                loop[i] = EditorGUILayout.Toggle("Loop", loop[i]);
                pingPong[i] = EditorGUILayout.Toggle("Ping Pong", pingPong[i]);
                EditorGUILayout.EndHorizontal();
                EditorGUILayout.Separator();
            }

            if (GUILayout.Button("Create"))
            {

                // My C# professor would kill me if I made a line of code like this...
                UnityEditor.Animations.AnimatorController controller = UnityEditor.Animations.AnimatorController.CreateAnimatorControllerAtPath(("Assets/" + controllerName + ".controller"));

                for (int i = 0; i < numAnimations; i++)
                {
                    AnimationClip tempClip = CreateClip(selectedObject, animationNames[i], startFrames[i], endFrames[i], clipFrameRate[i], clipTimeBetween[i], pingPong[i]);

                    if (loop[i])
                    {
                        AnimationClipSettings settings = AnimationUtility.GetAnimationClipSettings(tempClip);
                        settings.loopTime = true;
                        settings.loopBlend = true;
                        AnimationUtility.SetAnimationClipSettings(tempClip, settings);
                    }
                    controller.AddMotion(tempClip);
                }
            }
        }
        else
            Debug.Log("Null game Object! Are you sure you have an object selected?");
    }


    public AnimationClip CreateClip(Object obj, string clipName, int startFrame, int endFrame, float frameRate, float clipTimeBetween, bool pingPong)
    {
        string path = AssetDatabase.GetAssetPath(obj);

        Object[] sprites = AssetDatabase.LoadAllAssetsAtPath(path);

        int frameCount = endFrame - startFrame + 1;
        float frameLength = 1f / clipTimeBetween;

        AnimationClip clip = new AnimationClip();

        clip.frameRate = frameRate;
        EditorCurveBinding curveBinding = new EditorCurveBinding();
        curveBinding.type = typeof(SpriteRenderer);
        curveBinding.propertyName = "m_Sprite";

        ObjectReferenceKeyframe[] keyFrames;

        if (!pingPong)
            keyFrames = new ObjectReferenceKeyframe[frameCount + 1];
        else
            keyFrames = new ObjectReferenceKeyframe[frameCount * 2 + 1];

        int frameNumber = 0;

        for (int i = startFrame; i < endFrame + 1; i++, frameNumber++)
        {
            ObjectReferenceKeyframe tempKeyFrame = new ObjectReferenceKeyframe();
            tempKeyFrame.time = frameNumber * frameLength;
            tempKeyFrame.value = sprites[i];
            keyFrames[frameNumber] = tempKeyFrame;
        }

        if (pingPong)
        {
            for (int i = endFrame; i >= startFrame; i--, frameNumber++)
            {
                ObjectReferenceKeyframe tempKeyFrame = new ObjectReferenceKeyframe();
                tempKeyFrame.time = frameNumber * frameLength;
                tempKeyFrame.value = sprites[i];
                keyFrames[frameNumber] = tempKeyFrame;
            }
        }
        ObjectReferenceKeyframe lastSprite = new ObjectReferenceKeyframe();
        lastSprite.time = frameNumber * frameLength;
        lastSprite.value = sprites[startFrame];
        keyFrames[frameNumber] = lastSprite;

        clip.name = clipName;
        AnimationUtility.SetObjectReferenceCurve(clip, curveBinding, keyFrames);

        AssetDatabase.CreateAsset(clip, ("Assets/" + clipName + ".anim"));
        return clip;
    }



    void OnFocus()
    {
        if (EditorApplication.isPlayingOrWillChangePlaymode)
            this.Close();
    }
}


/*On your own
*Create a utility that will allow the user to select a sprite sheet (which has been sliced into multiple sprites), and then create animations from that sprite sheet.
*Each animation should have its own options for:
*
*    Looping
*    Ping-ponging
*    Start Frame
*    End Frame
*
*Make sure that the end frame cannot be higher than the number of frames the sprite sheet has been broken up into! (Hint: you may to have to research the function you use to load the sprites)
*Once the animations are created, create a sprite in the scene view at 0,0,0 with the newly created animation controller added to it.
*Make the sprite a prefab in the main assets folder.
*/


//Post-Lab Questions
//Difference between EditorGUILayout and EditorGUI?
//GUILayout automatically formats items, while GUI requires you to specificy the size, spacing, and formatting of all aspects of your custom menus.

//What is the OnGUI function?
//It controls how a GUI Window looks like when it is displayed

//Do variables need to be initialized before they can be used in GUI items?
//No - variables do not need to be initalized to a value before they are used in GUI items.

